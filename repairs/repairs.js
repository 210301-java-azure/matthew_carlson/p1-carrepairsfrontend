
/*
====================================================================================
load all repairs onto the page
====================================================================================
*/
ajaxGetUserRepairs(getAllSuccessCallback, undefined, sessionStorage.getItem("token"));

function getAllSuccessCallback(xhr) {
    addRepairsToRepairTable(JSON.parse(xhr.responseText));
}

function addRepairsToRepairTable(repairs) {
    let tbody = document.getElementById("repair-table-body");
    for (let repair of repairs) {
        let newRow = document.createElement("tr");
        // add td for each element of the repair
        for (let property in repair) {
            // don't display repair id number in table
            if (property != "id" && property != 'account') {
                let data = document.createElement("td");
                if ((property == "dateSubmitted" || property == "dateCompleted") && repair[property] != null) {
                    data.textContent = new Date(Number(repair[property]));
                } else if (property == "status") {
                    if (repair[property] == "IN_PROGRESS") {
                        data.textContent = "In Progress"
                    }
                    else {
                        data.textContent = repair[property].substr(0, 1).toUpperCase().concat(repair.status.toLowerCase().substring(1));
                    }
                } else {
                    data.textContent = repair[property];
                }
                newRow.appendChild(data);
            }
        }
        // setup info modal
        newRow.setAttribute("data-bs-toggle", "modal");
        newRow.setAttribute("data-bs-target", "#repair-info-modal");
        newRow.setAttribute("data-bs-whatever", repair.id);
        newRow.setAttribute("id", `table-row-for-repair-${repair.id}`)
        tbody.appendChild(newRow);
    }
    document.getElementById("repair-table").hidden = false;
}

/*
====================================================================================
repair info modal
====================================================================================
*/
document.getElementById('repair-info-modal').addEventListener('show.bs.modal', function (event) {
    let row = event.relatedTarget;
    // Get the repair ID from the data-bs-whatever attribute
    let repairId = row.getAttribute('data-bs-whatever');

    ajaxGetUserSingleRepair(repairId, modalSuccessCallback, undefined, sessionStorage.getItem("token"));

    function modalSuccessCallback(xhr) {
        fillRepairModalValues(JSON.parse(xhr.responseText));
    }
})


// Remove repair preview from the modal when it is dismissed
document.getElementById('repair-info-modal').addEventListener('hidden.bs.modal', (event) => {
    document.getElementById('repair-info-modal-make').textContent = '';
    document.getElementById('repair-info-modal-model').textContent = '';
    document.getElementById('repair-info-modal-year').textContent = '';
    document.getElementById('repair-info-modal-date-submitted').textContent = '';
    document.getElementById('repair-info-modal-date-completed').textContent = '';
    document.getElementById('repair-info-modal-price').textContent = '';
    document.getElementById('repair-info-modal-status').textContent = '';
    document.getElementById('repair-info-modal-problem').textContent = '';
    document.getElementById('repair-info-modal-solution').textContent = '';
})

function fillRepairModalValues(repair) {

    document.getElementById('repair-info-modal-make').textContent = repair.make;
    document.getElementById('repair-info-modal-model').textContent = repair.model;
    document.getElementById('repair-info-modal-year').textContent = repair.year;
    // avoid a null value becoming jan 1st 1970
    if (repair.dateSubmitted != null) {
        document.getElementById('repair-info-modal-date-submitted').textContent = new Date(Number(repair.dateSubmitted));
    }
    if (repair.dateCompleted != null) {
        document.getElementById('repair-info-modal-date-completed').textContent = new Date(Number(repair.dateCompleted));
    }
    document.getElementById('repair-info-modal-price').textContent = `$${repair.price}`;
    document.getElementById('repair-info-modal-problem').textContent = repair.problem;
    document.getElementById('repair-info-modal-solution').textContent = repair.solution;
    if (repair.status == 'IN_PROGRESS') {
        document.getElementById('repair-info-modal-status').textContent = 'In Progress';
    } else {
        document.getElementById('repair-info-modal-status').textContent = repair.status.substr(0, 1).toUpperCase().concat(repair.status.toLowerCase().substring(1));
    }

}

/*
====================================================================================
filtered search
====================================================================================
*/

document.getElementById('filtered-search-form').addEventListener('submit', (event) => {
    event.preventDefault();

    const status = document.getElementById('filtered-search-form-status').value;
    const problem = document.getElementById('filtered-search-form-problem').value;
    let urlString;
    if (status) {
        if (problem) {
            // both
            urlString = `${serverBaseUrl}/repairs?status=${status}&problem=${problem}`
        } else {
            // status
            urlString = `${serverBaseUrl}/repairs?status=${status}`
        }
    } else if (problem) {
        // problem
        urlString = `${serverBaseUrl}/repairs?problem=${problem}`
    } else {
        // no filtering
        const tbody = document.getElementById('repair-table-body');
        // clear current table
        tbody.textContent = ''
        ajaxGetUserRepairs(getAllSuccessCallback, undefined, sessionStorage.getItem("token"));
        return;
    }
    fetch(urlString, { headers: { 'Authorization': sessionStorage.getItem('token') } })
        .then(response => response.json())
        .then(remakeTable)
        .catch(e => console.error(e))

    function remakeTable(repairs) {
        const tbody = document.getElementById('repair-table-body');
        // clear current table
        tbody.textContent = ''
        // fill the table again
        addRepairsToRepairTable(repairs);
    }
})
