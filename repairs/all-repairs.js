
// onblur of each form element calls this
function automaticValidation(el) {
	el.parentElement.classList.add("was-validated")
}

/*
====================================================================================
load all repairs onto the page
====================================================================================
*/
ajaxGetAllRepairs(getAllSuccessCallback, getAllFailureCallback, sessionStorage.getItem("token"));

function getAllSuccessCallback(xhr) {
	addRepairsToAllRepairTable(JSON.parse(xhr.responseText));
}

function getAllFailureCallback() {
	// ideally show unauth page, maybe just a message for now.
}

function addRepairsToAllRepairTable(repairs) {
	let tbody = document.getElementById("repair-table-body");
	for (let repair of repairs) {
		let newRow = document.createElement("tr");
		// add td for each element of the repair
		for (let property in repair) {
			// don't display repair id number in table
			if (property != "id") {
				let data = document.createElement("td");
				if (property == "account") {
					data.textContent = `${repair.account.firstName} ${repair.account.lastName}`;
				} else if ((property == "dateSubmitted" || property == "dateCompleted") && repair[property] != null) {
					data.textContent = new Date(Number(repair[property]));
				} else if (property == "status") {
					if (repair[property] == "IN_PROGRESS") {
						data.textContent = "In Progress"
					}
					else {
						data.textContent = repair[property].substr(0, 1).toUpperCase().concat(repair[property].toLowerCase().substring(1));
					}
				} else {
					data.textContent = repair[property];
				}
				// place modal toggle on td so delete button doesn't trigger modal
				data.setAttribute("data-bs-toggle", "modal");
				data.setAttribute("data-bs-target", "#updateRepairModal");
				data.setAttribute("data-bs-whatever", repair.id);
				newRow.appendChild(data);
			}
		}
		// add delete button
		let delButtonTd = document.createElement("td");
		let delButton = document.createElement("button");
		let delButtonIcon = document.createElement('i');
		delButtonIcon.setAttribute("class", "bi bi-trash-fill");
		delButtonIcon.setAttribute("width", "16")
		delButtonIcon.setAttribute("height", "16")
		delButtonIcon.setAttribute('role', 'img')
		delButtonIcon.setAttribute('aria-label', 'delete')
		delButton.classList.add('btn', 'btn-outline-secondary')
		delButton.setAttribute("data-bs-toggle", "modal");
		delButton.setAttribute("data-bs-target", "#delete-confirmation-modal");
		delButton.setAttribute("data-bs-whatever", repair.id);
		delButton.appendChild(delButtonIcon);
		delButton.innerHTML = delButton.innerHTML + "Delete";
		delButtonTd.appendChild(delButton);
		newRow.appendChild(delButtonTd);
		// set row id
		newRow.setAttribute("id", `table-row-for-repair-${repair.id}`)
		tbody.appendChild(newRow);
	}
	document.getElementById("repair-table").hidden = false;
}

/*
====================================================================================
update modal
====================================================================================
*/
document.getElementById('updateRepairModal').addEventListener('show.bs.modal', function (event) {
	let row = event.relatedTarget;
	// Get the repair ID from the data-bs-whatever attribute
	let repairId = row.getAttribute('data-bs-whatever');

	ajaxGetAdminSingleRepair(repairId, modalSuccessCallback, modalFailureCallback, sessionStorage.getItem("token"));

	document.getElementById("updateRepairForm").addEventListener("submit", updateRepair);
})

function modalSuccessCallback(xhr) {
	fillRepairModalValues(JSON.parse(xhr.responseText));
}

function modalFailureCallback() {
	// close the modal I guess?
}

// Fill the account select drop down once on page load.
// value is the account id
fetch(`${serverBaseUrl}/accounts`, { headers: { Authorization: sessionStorage.getItem('token') } })
	.then(response => response.json())
	.then((accounts) => {
		const selectElem = document.getElementById("updateRepairForm-account");
		for (let account of accounts) {
			let newOption = document.createElement("option");
			newOption.value = account.id;
			newOption.setAttribute('id', `account-select-option-${account.id}`)
			newOption.textContent = `${account.firstName} ${account.lastName}`;
			selectElem.appendChild(newOption);
		}
	})
	.catch(error => {
		console.error(error);
		const response = document.getElementById("create-form-submission-response")
		let h = document.createElement("h3")
		h.textContent = `Failed to fetch the list of accounts\nPlease refresh`
		h.classList.add("text-danger")
		response.appendChild(h)
		response.hidden = false;
	})


// Remove validation messages from the modal when it is dismissed
document.getElementById('updateRepairModal').addEventListener('hidden.bs.modal', (event) => {
	const divs = document.querySelectorAll('.was-validated')
	for (let div of divs) {
		div.classList.remove('was-validated')
	}
	const message = document.getElementById('updateRepairModalLabel')
	message.classList.remove('text-success', 'text-danger')
	message.textContent = "Update Repair Details"
	// maybe null out the values to ensure no stale data
})

function fillRepairModalValues(repair) {
	document.getElementById('updateRepairForm-id').value = repair.id;
	document.getElementById('updateRepairForm-account').value = repair.account.id;
	document.getElementById('updateRepairForm-make').value = repair.make;
	document.getElementById('updateRepairForm-model').value = repair.model;
	document.getElementById('updateRepairForm-year').value = repair.year;
	// avoid a null value becoming jan 1st 1970
	if (repair.dateSubmitted != null) {
		document.getElementById('updateRepairForm-date-submitted').value = getDatetimeStringFromDate(new Date(Number(repair.dateSubmitted)));
	} else {
		document.getElementById('updateRepairForm-date-submitted').value = null;
	}
	if (repair.dateCompleted != null) {
		document.getElementById('updateRepairForm-date-completed').value = getDatetimeStringFromDate(new Date(Number(repair.dateCompleted)));
	} else {
		document.getElementById('updateRepairForm-date-completed').value = null;
	}
	document.getElementById('updateRepairForm-price').value = repair.price;
	document.getElementById('updateRepairForm-problem').value = repair.problem;
	document.getElementById('updateRepairForm-solution').value = repair.solution;
	document.getElementById('updateRepairForm-status').value = repair.status;

	// Filling the datetime field while maintaining the timezone
	// using date.toISOString() converts to UTC
	function getDatetimeStringFromDate(date) {
		let dateString = date.getFullYear().toString() + '-' + (date.getMonth() + 1).toString().padStart(2, '0') + '-' + date.getDate().toString().padStart(2, '0');
		let timeString = date.getHours().toString().padStart(2, '0') + ':' + date.getMinutes().toString().padStart(2, '0') + ':' + date.getSeconds().toString().padStart(2, '0');
		return dateString + "T" + timeString
	}

}


function updateRepair(event) {
	event.preventDefault();

	const repairBody = {
		"id": document.getElementById("updateRepairForm-id").value,
		"account": {
			"id": document.getElementById("updateRepairForm-account").value,
		},
		"make": document.getElementById("updateRepairForm-make").value,
		"model": document.getElementById("updateRepairForm-model").value,
		"year": document.getElementById("updateRepairForm-year").value,
		"dateSubmitted": document.getElementById("updateRepairForm-date-submitted").value,
		"dateCompleted": document.getElementById("updateRepairForm-date-completed").value,
		"price": document.getElementById("updateRepairForm-price").value,
		"problem": document.getElementById("updateRepairForm-problem").value,
		"solution": document.getElementById("updateRepairForm-solution").value,
		"status": document.getElementById("updateRepairForm-status").value
	}
	// Save the name for the callback to update the table
	const updatedNameForTable = document.getElementById(`account-select-option-${repairBody.account.id}`).textContent;

	fetch(`${serverBaseUrl}/all-repairs/${repairBody.id}`, {
		method: 'PUT',
		headers: {
			'Authorization': sessionStorage.getItem('token'),
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(repairBody)
	})
		.then(response => response.json())
		.then(repair => {
			let modalHeader = document.getElementById("updateRepairModalLabel");
			modalHeader.textContent = "Update Successful";
			modalHeader.classList.add('text-success');
			// update the table, don't require a page refresh
			const rowElements = document.getElementById(`table-row-for-repair-${repair.id}`).childNodes;
			rowElements[0].textContent = updatedNameForTable;
			rowElements[1].textContent = repair.make;
			rowElements[2].textContent = repair.model;
			rowElements[3].textContent = repair.year;
			if (repair.dateSubmitted != null) {
				rowElements[4].textContent = new Date(Number(repair.dateSubmitted))
			} else {
				rowElements[4].textContent = null;
			}
			if (repair.dateCompleted != null) {
				rowElements[5].textContent = new Date(Number(repair.dateCompleted))
			} else {
				rowElements[5].textContent = null;
			}
			rowElements[6].textContent = repair.price;
			rowElements[7].textContent = repair.problem;
			rowElements[8].textContent = repair.solution;
			rowElements[9].textContent = repair.status;
		})
		.catch(error => {
			console.error(error)
			let modalHeader = document.getElementById("updateRepairModalLabel");
			modalHeader.textContent = "Update Failed"
			modalHeader.classList.add('text-danger')
		})


	// Can't seem to hide the modal. Just changing the header instead
}

/*
====================================================================================
delete modal
====================================================================================
*/

document.getElementById("delete-confirmation-modal").addEventListener("show.bs.modal", function (event) {
	let delButton = event.relatedTarget;
	// Get the repair ID from the data-bs-whatever attribute
	let repairId = delButton.getAttribute('data-bs-whatever');

	ajaxGetAdminSingleRepair(repairId, fillDeleteModal, modalFailureCallback, sessionStorage.getItem("token"));

	function fillDeleteModal(xhr) {
		const repair = JSON.parse(xhr.responseText);
		document.getElementById('delete-modal-name').textContent = `${repair.account.firstName} ${repair.account.lastName}`;
		document.getElementById('delete-modal-make').textContent = repair.make;
		document.getElementById('delete-modal-model').textContent = repair.model;
		document.getElementById('delete-modal-year').textContent = repair.year;
		if (repair.status == 'IN_PROGRESS') {
			document.getElementById('delete-modal-status').textContent = 'In Progress';
		} else {
			document.getElementById('delete-modal-status').textContent = repair.status.substr(0, 1).toUpperCase().concat(repair.status.toLowerCase().substring(1));
		}
		document.getElementById('delete-modal-problem').textContent = repair.problem;
		document.getElementById('delete-modal-solution').textContent = repair.solution;
		document.getElementById('delete-modal-date-submitted').textContent = new Date(Number(repair.dateSubmitted));
	}

	document.getElementById("delete-modal-button").addEventListener("click", deleteRepair);

	function deleteRepair() {
		fetch(`${serverBaseUrl}/all-repairs/${repairId}`, {
			method: 'DELETE',
			headers: {
				'Authorization': sessionStorage.getItem('token')
			}
		})
			.then((response) => {
				if (response.ok) {
					const modalHeader = document.getElementById('delete-confirmation-modal-label');
					modalHeader.textContent = 'Successfully deleted the repair'
					modalHeader.classList.add('text-success');
					// remove the row from the table
					delButton.parentElement.parentElement.remove();
				} else {
					const modalHeader = document.getElementById('delete-confirmation-modal-label');
					modalHeader.textContent = 'Failed to delete the repair'
					modalHeader.classList.add('text-danger');
				}
			})
	}
})


// Remove repair preview from the modal when it is dismissed
document.getElementById('updateRepairModal').addEventListener('hidden.bs.modal', (event) => {
	document.getElementById('delete-modal-name').textContent = ''
	document.getElementById('delete-modal-make').textContent = '';
	document.getElementById('delete-modal-model').textContent = '';
	document.getElementById('delete-modal-year').textContent = '';
	document.getElementById('delete-modal-status').textContent = '';
	document.getElementById('delete-modal-problem').textContent = '';
	document.getElementById('delete-modal-solution').textContent = '';
	document.getElementById('delete-modal-date-submitted').textContent = '';
})


/*
====================================================================================
Filtered search
====================================================================================
*/

document.getElementById('filtered-search-form').addEventListener('submit', (event) => {
	event.preventDefault();

	const status = document.getElementById('filtered-search-form-status').value;
	const problem = document.getElementById('filtered-search-form-problem').value;
	let urlString;
	// I believe there's a way to programatically define the query params, but it seemed
	// to not be consistently supported
	if (status) {
		if (problem) {
			// both
			urlString = `${serverBaseUrl}/all-repairs?status=${status}&problem=${problem}`
		} else {
			// status
			urlString = `${serverBaseUrl}/all-repairs?status=${status}`
		}
	} else if (problem) {
		// problem
		urlString = `${serverBaseUrl}/all-repairs?problem=${problem}`
	} else {
		// no filtering
		const tbody = document.getElementById('repair-table-body');
		// clear current table
		tbody.textContent = ''
		ajaxGetAllRepairs(getAllSuccessCallback, undefined, sessionStorage.getItem("token"));
		return;
	}
	fetch(urlString, { headers: { 'Authorization': sessionStorage.getItem('token') } })
		.then(response => response.json())
		.then(remakeTable)
		.catch(e => console.error(e))

	function remakeTable(repairs) {
		const tbody = document.getElementById('repair-table-body');
		// clear current table
		tbody.textContent = ''
		// fill the table again
		addRepairsToAllRepairTable(repairs);
	}
})
