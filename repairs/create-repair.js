
document.getElementById('create-repair-form').addEventListener("submit", submitNewRepair);

fetch(`${serverBaseUrl}/accounts`, { headers: { Authorization: sessionStorage.getItem('token') } })
	.then(response => response.json())
	.then(fillCustomersSelect)
	.catch(error => {
		console.error(error);
		const response = document.getElementById("create-form-submission-response")
		let h = document.createElement("h3")
		h.textContent = `Failed to fetch the list of accounts\nPlease refresh`
		h.classList.add("text-danger")
		response.appendChild(h)
		response.hidden = false;
	})


function submitNewRepair(event) {
	event.preventDefault();

	const createdRepair = {
		account: {
			id: document.getElementById("create-repair-form-account").value,
		},
		price: document.getElementById("create-repair-form-price").value,
		status: document.getElementById("create-repair-form-status").value,
		make: document.getElementById("create-repair-form-make").value,
		model: document.getElementById("create-repair-form-model").value,
		year: document.getElementById("create-repair-form-year").value,
		problem: document.getElementById("create-repair-form-problem").value,
		solution: document.getElementById("create-repair-form-solution").value
	}

	console.log(createdRepair)
	fetch(`${serverBaseUrl}/all-repairs`, {
		method: "POST",
		headers: {
			'Authorization': sessionStorage.getItem('token'),
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(createdRepair)
	})
		.then(response => response.json())
		.then(newRepairSuccess)
		.catch(e => {
			console.error(e);
			const response = document.getElementById("create-form-submission-response")
			let h = document.createElement("h3")
			h.textContent = `Something went wrong, repair not created`
			h.classList.add("text-danger")
			response.appendChild(h)
			response.hidden = false;
		});
	// in success callback display success message
}

// onblur of each form element calls this
function automaticValidation(el) {
	el.parentElement.classList.add("was-validated")
}

function fillCustomersSelect(accounts) {
	const selectElem = document.getElementById("create-repair-form-account");
	for (let account of accounts) {
		let newOption = document.createElement("option");
		newOption.value = account.id;
		newOption.textContent = `${account.firstName} ${account.lastName}`;
		selectElem.appendChild(newOption);
	}
}

function newRepairSuccess(repair) {
	console.log("success");
	console.log(repair)
	const response = document.getElementById("create-form-submission-response")
	let h = document.createElement("h3")
	h.textContent = `Successfully created the repair`
	h.classList.add("text-success")
	response.appendChild(h)
	response.hidden = false;
}