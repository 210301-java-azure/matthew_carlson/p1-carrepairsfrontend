# Car Repairs Frontend
## Try it live
[http location of the frontend hosted on Azure blob](http://mcarlsontraining.blob.core.windows.net/car-repairs-frontend/index.html)

## Navigation Bar
When not logged in, you can navigate to [home](#home), [Log in](#login), and [Create Account](#create-account)

When a customer is logged in, they have links to [home](#home), [repairs](#repairs), [account](#account), and [log out](#) which will log the user out while staying on the same page

When an admin is logged in, they have links to [home](#home), [repairs (all)](#repairs), [create repair](#create-repair), [account](#account), [accounts](#all-accounts), and [log out](#)

## Home
The home page when not logged in shows a basic message
![Home page image when not logged in](/documentation-images/index-not-logged-in.JPG)

After you log in, it shows a list of all of your repairs, to easily see information you would want quickly. The repairs are ordered by the most recently submitted repairs.
![Home page when logged in](/documentation-images/index-logged-in.JPG)

## Login
A basic login form page. On successful login it redirects to [home](#home) page. When you fail to login, a message is displayed.
![login page](/documentation-images/login.JPG)

## Create Account
Basic form to create a new account, featuring input validation while you fill it in.
![create account page](/documentation-images/create-account.JPG)

## Repairs
If you are a customer, you see your repairs, just like the home page, but you can filter search by status or the problem.
![repairs for a customer](/documentation-images/repairs-customer.JPG)

If you are an admin, you can see every repair, and filter by status or problem
![repairs for an admin](/documentation-images/repairs-admin.JPG)
And you can update or delete any of the repairs
![admin updating repairs](/documentation-images/repairs-admin-update.JPG)
![admin deleting repairs](/documentation-images/repairs-admin-delete.JPG)

## Create Repair
An admin can create new repairs, using a form on its own page
![creating a new repair](/documentation-images/create-repair.JPG)

## Account
A page available to admins and customers to edit their own accounts.
![account page](/documentation-images/account.JPG)

## All Accounts
A page for admins to view, edit, and delete all accounts in the system.
![accounts page](/documentation-images/accounts.JPG)
![accounts page update](/documentation-images/accounts-update.JPG)


