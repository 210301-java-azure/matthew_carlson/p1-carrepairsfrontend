/**
 * If user is logged in, retrieve all repairs for logged in user and display in a table
 * If not, the page remains.
 */
ajaxGetUserRepairs(indexSuccessCallback, () => { }, sessionStorage.getItem("token"));

function indexSuccessCallback(xhr) {
    addRepairsToIndexTable(JSON.parse(xhr.responseText));
}

function addRepairsToIndexTable(repairs) {
    let tbody = document.getElementById("index-page-repair-table-body");
    for (let repair of repairs) {
        let newRow = document.createElement("tr");
        for (let property in repair) {
            // don't display repair or user id numbers
            if (property != "id" && property != "account") {
                let data = document.createElement("td");
                if ((property == "dateSubmitted" || property == "dateCompleted") && repair[property] != null) {
                    data.textContent = new Date(Number(repair[property]));
                } else if (property == "status") {
                    if (repair[property] == "IN_PROGRESS") {
                        data.textContent = "In Progress"
                    }
                    else {
                        data.textContent = repair[property].substr(0, 1).toUpperCase().concat(repair[property].toLowerCase().substring(1));;
                    }
                } else {
                    data.textContent = repair[property];
                }
                newRow.appendChild(data);
            }
        }
        tbody.appendChild(newRow);
    }
    document.getElementById("index-page-repair-table").hidden = false;
}

