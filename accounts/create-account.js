
document.getElementById('create-account-form').addEventListener('submit', submitNewAccount);

function submitNewAccount(event) {
    event.preventDefault();

    const createdAccount = {
        'firstName': document.getElementById('create-account-form-first-name').value,
        'lastName': document.getElementById('create-account-form-last-name').value,
        'email': document.getElementById('create-account-form-email').value,
        'password': document.getElementById('create-account-form-password').value
    }

    fetch(`${serverBaseUrl}/accounts`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(createdAccount)
    })
        .then(response => response.json())
        .then(() => {
            window.location.href = './login.html';
        })
        .catch(error => {
            const message = document.getElementById('error-msg')
            message.textContent = 'Something went wrong when creating the account'
            message.classList.add('text-danger')
            message.hidden = false;
        })
}

// onblur of each form element calls this
function automaticValidation(el) {
    el.parentElement.classList.add("was-validated")
}