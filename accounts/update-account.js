document.getElementById('update-account-form').addEventListener("submit", updateAccount);

fetch(`${serverBaseUrl}/accounts/${sessionStorage.getItem('uid')}`, { headers: { 'Authorization': sessionStorage.getItem('token') } })
	.then(response => response.json())
	.then(fillUpdateAccountForm)
	.catch(error => {
		console.error(error);
		logout();
		window.location.href = "./login.html"
	})

function fillUpdateAccountForm(account) {
	document.getElementById("update-account-form-first-name").value = account.firstName;
	document.getElementById("update-account-form-last-name").value = account.lastName;
	document.getElementById("update-account-form-email").value = account.email;
	document.getElementById("update-account-form-password").value = account.password;
	document.getElementById("update-account-form-role").value = account.role;
}

function updateAccount(event) {
	event.preventDefault();

	const updatedAccount = {
		'email': document.getElementById("update-account-form-email").value,
		'firstName': document.getElementById("update-account-form-first-name").value,
		'lastName': document.getElementById("update-account-form-last-name").value,
		'password': document.getElementById("update-account-form-password").value,
		'role': document.getElementById("update-account-form-role").value
	}
	fetch(`${serverBaseUrl}/accounts/${sessionStorage.getItem('uid')}`, {
		method: "PUT",
		headers: { 'Authorization': sessionStorage.getItem('token') },
		body: JSON.stringify(updatedAccount)
	})
		.then(response => response.json())
		.then(updateSuccess)
		.catch(error => {
			console.error(error);
			// display error message
			const message = document.getElementById("update-account-form-submission-response");
			message.textContent = "Update failed"
			message.classList.add("text-danger")
			message.hidden = false;
		})

	function updateSuccess(account) {
		// display success message
		const message = document.getElementById("update-account-form-submission-response");
		message.textContent = "Update successful"
		message.classList.add("text-success")
		message.hidden = false;
	}

}

// onblur of each form element calls this
function automaticValidation(el) {
	el.parentElement.classList.add("was-validated")
}

function deleteUser() {
	fetch(`${serverBaseUrl}/accounts/${sessionStorage.getItem('uid')}`, {
		method: 'DELETE',
		headers: { 'Authorization': sessionStorage.getItem('token') }
	})
		.then((response) => {
			if (response.ok) {
				logout();
				window.location.href = '../index.html'
			}
			else {
				const message = document.getElementById('delete-confirmation-modal-label')
				message.textContent = "Something went wrong deleting the account"
				message.classList.add('text-danger')
			}
		})
}