/*
=============================== page preparation =======================================
*/
fetch(`${serverBaseUrl}/accounts`, { headers: { 'Authorization': sessionStorage.getItem('token') } })
	.then(response => response.json())
	.then(fillAccountTable)
	.catch(error => {
		console.error(error);
		// possible feedback
	})

// onblur of each form element calls this
function automaticValidation(el) {
	el.parentElement.classList.add("was-validated")
}

function fillAccountTable(accounts) {
	const tbody = document.getElementById("account-table-body");
	for (let account of accounts) {
		let newRow = document.createElement("tr");
		for (let property in account) {
			if (property != "password" && property != 'id') {
				let data = document.createElement("td");
				if (property == 'role') {
					if (account[property] == 'ADMIN') {
						data.textContent = 'Admin';
					} else {
						data.textContent = 'Customer';
					}
				} else {
					data.textContent = account[property];
				}
				// place model trigger on td so delete doesn't trigger it
				data.setAttribute("data-bs-toggle", "modal");
				data.setAttribute("data-bs-target", "#update-account-modal");
				data.setAttribute("data-bs-whatever", account.id);
				newRow.appendChild(data);
			}
		}
		// add delete button
		let delButtonTd = document.createElement("td");
		let delButton = document.createElement("button")
		let delButtonIcon = document.createElement('i');
		delButtonIcon.setAttribute("class", "bi bi-trash-fill");
		delButtonIcon.setAttribute("width", "16")
		delButtonIcon.setAttribute("height", "16")
		delButtonIcon.setAttribute('role', 'img')
		delButtonIcon.setAttribute('aria-label', 'delete')
		delButton.classList.add('btn', 'btn-outline-secondary')
		delButton.setAttribute("data-bs-toggle", "modal");
		delButton.setAttribute("data-bs-target", "#delete-confirmation-modal");
		delButton.setAttribute("data-bs-whatever", account.id);
		delButton.appendChild(delButtonIcon);
		delButton.innerHTML = delButton.innerHTML + "Delete";
		delButtonTd.appendChild(delButton);
		newRow.appendChild(delButtonTd);
		// set row id
		newRow.setAttribute("id", `table-row-for-account-${account.id}`);
		tbody.appendChild(newRow);
	}
	document.getElementById("account-table").hidden = false;
}

/*
=============================== modal setup ========================================
*/
document.getElementById('update-account-modal').addEventListener('show.bs.modal', (event) => {
	let row = event.relatedTarget;

	fetch(`${serverBaseUrl}/accounts/${row.getAttribute('data-bs-whatever')}`, {
		headers: { 'Authorization': sessionStorage.getItem('token') }
	})
		.then(response => response.json())
		.then(fillUpdateAccountModal)
		.catch(error => {
			console.error(error);
			// error message
		})

	function fillUpdateAccountModal(account) {
		document.getElementById('update-account-form-id').value = account.id;
		document.getElementById('update-account-form-password').value = account.password;
		document.getElementById('update-account-form-first-name').value = account.firstName;
		document.getElementById('update-account-form-last-name').value = account.lastName;
		document.getElementById('update-account-form-email').value = account.email;
		document.getElementById('update-account-form-role').value = account.role;
	}
})

document.getElementById('update-account-form').addEventListener('submit', (event) => {
	event.preventDefault();
	const updatedAccount = {
		'id': document.getElementById('update-account-form-id').value,
		'password': document.getElementById('update-account-form-password').value,
		'firstName': document.getElementById('update-account-form-first-name').value,
		'lastName': document.getElementById('update-account-form-last-name').value,
		'email': document.getElementById('update-account-form-email').value,
		'role': document.getElementById('update-account-form-role').value
	}

	fetch(`${serverBaseUrl}/accounts/${updatedAccount.id}`, {
		method: "PUT",
		headers: {
			'Authorization': sessionStorage.getItem('token')
		},
		body: JSON.stringify(updatedAccount)
	})
		.then(response => response.json())
		.then(updateSuccess)
		.catch(e => {
			console.error(e);
			// error message
			let modalHeader = document.getElementById('update-account-modal-label');
			modalHeader.textContent = 'Update Failed';
			modalHeader.classList.add('text-danger');
		})
	function updateSuccess(account) {
		// success message
		let modalHeader = document.getElementById('update-account-modal-label');
		modalHeader.textContent = 'Update Successful';
		modalHeader.classList.add('text-success');
		// update the table
		const rowData = document.getElementById(`table-row-for-account-${account.id}`).childNodes;
		rowData[0].textContent = account.firstName;
		rowData[1].textContent = account.lastName;
		rowData[2].textContent = account.email;
		if (account.role == "ADMIN") {
			rowData[3].textContent = "Admin";
		} else {
			rowData[3].textContent = "Customer";
		}
	}
	// fetch
})

// Remove validation messages from the modal when it is dismissed
document.getElementById('update-account-modal').addEventListener('hidden.bs.modal', (event) => {
	const divs = document.querySelectorAll('.was-validated')
	for (let div of divs) {
		div.classList.remove('was-validated')
	}
	const message = document.getElementById('update-account-modal-label')
	message.classList.remove('text-success', 'text-danger')
	message.textContent = "Update Account Details"
})


// ========================= delete modal ================================================

document.getElementById('delete-confirmation-modal').addEventListener('show.bs.modal', function (event) {
	let delButton = event.relatedTarget;
	// Get the repair ID from the data-bs-whatever attribute
	let accountId = delButton.getAttribute('data-bs-whatever');

	fetch(`${serverBaseUrl}/accounts/${accountId}`, { headers: { 'Authorization': sessionStorage.getItem('token') } })
		.then(response => response.json())
		.then(fillDeleteModal)
		.catch(e => console.error(e))

	function fillDeleteModal(account) {
		document.getElementById('delete-modal-first-name').textContent = account.firstName;
		document.getElementById('delete-modal-last-name').textContent = account.lastName;
		document.getElementById('delete-modal-email').textContent = account.email;
	}

	document.getElementById('delete-modal-button').addEventListener('click', function () {

		fetch(`${serverBaseUrl}/accounts/${accountId}`, {
			method: 'DELETE',
			headers: {
				'Authorization': sessionStorage.getItem('token')
			}
		})
			.then((response) => {
				if (response.ok) {
					const modalHeader = document.getElementById('delete-confirmation-modal-label');
					modalHeader.textContent = 'Successfully deleted the account'
					modalHeader.classList.add('text-success');
					// remove the row from the table
					delButton.parentElement.parentElement.remove();
				} else {
					const modalHeader = document.getElementById('delete-confirmation-modal-label');
					modalHeader.textContent = 'Failed to delete the account'
					modalHeader.classList.add('text-danger');
				}
			})
	})
})


