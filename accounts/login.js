document.getElementById("login-form").addEventListener("submit", attemptLogin);

function attemptLogin(event) {
    event.preventDefault(); // this stops the form from sending a GET request by default to the same URL
    const email = document.getElementById("inputEmail").value;
    const password = document.getElementById("inputPassword").value;
    ajaxLogin(email, password, successfulLogin, loginFailed)
}

function successfulLogin(xhr) {
    const authToken = xhr.getResponseHeader("Authorization");
    sessionStorage.setItem("token", authToken);
    sessionStorage.setItem('uid', xhr.getResponseHeader("User-Id"))
    window.location.href = "../index.html";
}

function loginFailed(xhr) {
    // console.log("oh no something went wrong");
    const errorDiv = document.getElementById("error-msg");
    errorDiv.hidden = false;
    errorDiv.innerText = xhr.responseText;
    errorDiv.classList.add('text-danger')
}