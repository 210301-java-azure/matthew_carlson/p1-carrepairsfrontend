
// const serverBaseUrl = "http://localhost:80"
const serverBaseUrl = "http://52.149.161.154"
const indexPage = ``

function sendAjaxRequest(method, url, body, successCallback, failureCallback, authToken) {
	const xhr = new XMLHttpRequest();
	xhr.open(method, url);
	if (authToken) {
		xhr.setRequestHeader("Authorization", authToken);
	}
	xhr.onreadystatechange = function () {
		if (xhr.readyState == 4) {
			if (xhr.status > 199 && xhr.status < 300) {
				successCallback(xhr);
			} else {
				failureCallback(xhr);
			}
		}
	}
	if (body) {
		xhr.send(body);
	} else {
		xhr.send();
	}
}

function sendAjaxGet(url, successCallback, failureCallback, authToken) {
	sendAjaxRequest("GET", url, undefined, successCallback, failureCallback, authToken);
}

function sendAjaxPost(url, body, successCallback, failureCallback, authToken) {
	sendAjaxRequest("POST", url, body, successCallback, failureCallback, authToken);
}

function sendAjaxPut(url, body, successCallback, failureCallback, authToken) {
	sendAjaxRequest("PUT", url, body, successCallback, failureCallback, authToken);
}

function ajaxGetAllRepairs(successCallback, failureCallback, authToken) {
	sendAjaxGet(`${serverBaseUrl}/all-repairs`, successCallback, failureCallback, authToken);
}

function ajaxGetUserRepairs(successCallback, failureCallback, authToken) {
	sendAjaxGet(`${serverBaseUrl}/repairs`, successCallback, failureCallback, authToken);
}

function ajaxGetUserSingleRepair(repairId, successCallback, failureCallback, authToken) {
	sendAjaxGet(`${serverBaseUrl}/repairs/${repairId}`, successCallback, failureCallback, authToken);
}

function ajaxGetAdminSingleRepair(id, successCallback, failureCallback, authToken) {
	sendAjaxGet(`${serverBaseUrl}/all-repairs/${id}`, successCallback, failureCallback, authToken);
}

function ajaxLogin(email, password, successCallback, failureCallback) {
	const payload = `email=${email}&password=${password}`;
	sendAjaxPost(`${serverBaseUrl}/login`, payload, successCallback, failureCallback);
}

// maybe return userId / role as header again for frontend
function validateToken(successCallback, failureCallback) {
	// attempt an admin-only action. Returns status 200 for admin (success), 401 for user, and 400 for bad token (failures)
	ajaxGetAdminSingleRepair(0, successCallback, failureCallback, sessionStorage.getItem("token"));
}

/*
function ajaxCreateItem(item, successCallback, failureCallback){
	const itemJson = JSON.stringify(item);
	const auth = sessionStorage.getItem("token");
	sendAjaxPost("http://localhost:7000/items", itemJson, successCallback, failureCallback, auth);
}
 */
