/*
	MUST HAVE SERVER-REQUESTS.JS ADDED BEFORE THIS
		(Uses validateToken as soon as script is loaded)
*/


if (sessionStorage.getItem("token")) {
	// if there is a token, we allow users to create a new item and to log out, if not we allow them to log in
	document.getElementById("login-nav-item").hidden = true;
	document.getElementById("create-account-nav-item").hidden = true;
	document.getElementById("account-nav-item").hidden = false;
	document.getElementById("logout-nav-item").hidden = false;
	document.getElementById("repairs-nav-item").hidden = false;
	function adminCallback() {
		if (document.getElementById('index-page-repair-table')) {
			document.getElementById('repairs-nav-item-link').setAttribute('href', './repairs/all-repairs.html')
		} else {
			document.getElementById("repairs-nav-item-link").setAttribute("href", "../repairs/all-repairs.html");
		}
		document.getElementById("create-repair-nav-item").hidden = false;
		document.getElementById("all-accounts-nav-item").hidden = false;
	}
	function userCallback(xhr) {
		if (xhr.status == 401) {
			console.log("user valid")
			if (document.getElementById('index-page-repair-table')) {
				document.getElementById("repairs-nav-item-link").setAttribute("href", "./repairs/repairs.html");
			} else {
				document.getElementById("repairs-nav-item-link").setAttribute("href", "../repairs/repairs.html");
			}
		} else {
			console.log("bad token")
			logout();
		}
	}
	// May switch to storing user role in sessionStorage. Allows people to easily change that but they shouldn't
	// be able to actually access anything, since the jwt token controls access server-side
	validateToken(adminCallback, userCallback);

}

// associate the logout functionality with clicking on the logout item on the nav bar
document.getElementById("logout-nav-item").addEventListener("click", logout);

function logout() {
	sessionStorage.clear();
	document.getElementById("login-nav-item").hidden = false;
	document.getElementById("create-account-nav-item").hidden = false;
	document.getElementById("account-nav-item").hidden = true;
	document.getElementById("logout-nav-item").hidden = true;
	document.getElementById("repairs-nav-item").hidden = true;
	document.getElementById("create-repair-nav-item").hidden = true;
	document.getElementById("all-accounts-nav-item").hidden = true;
}
